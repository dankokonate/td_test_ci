class CartePizzeria:

    def __init__(self):
        self.pizzas = []

    def is_empty(self):
        if len(self.pizzas) == 0:
            return True
        return False

    def nb_pizza(self):
        return len(self.pizzas)

    def add_pizza(self, pizza):
        self.pizzas.append(pizza)

    def remove_pizza(self, name):

        found = False
        indice = 0
        for pizza in self.pizzas:
            if pizza.name == name:
                found = True
                break
            indice += 1

        if found:
            del self.pizzas[indice]
        else:
            raise Exception("ta pizza existe pas ((:")

        return self.remove_pizza()
